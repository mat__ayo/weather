import { StatusBar } from "expo-status-bar";
import { StyleSheet, View } from "react-native";
import { store } from "./src/app/store";
import { Provider } from "react-redux";
import AppLoading from "expo-app-loading";
import { useFonts } from "expo-font";

//-> component imports
import Routes from "./src/Routes";

export default function App() {
  //-> configure fonts
  let [fontsLoaded] = useFonts({
    NunitoRegular: require("./assets/fonts/NunitoRegular.ttf"),
    NunitoBold: require("./assets/fonts/NunitoBold.ttf"),
  });

  if (!fontsLoaded) {
    return <AppLoading />;
  } else {
    return (
      <Provider store={store}>
        <View style={styles.container}>
          <Routes />
        </View>
      </Provider>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
  },
});
