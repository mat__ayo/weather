import React, { useEffect } from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import { useDispatch } from "react-redux";

//-> component imports
import HomeScreen from "./components/home/HomeScreen";
import DetailsScreen from "./components/details/DetailsScreen";
import { fetchWeather } from "./actions/fetchWeather";

//-> Navigation Stack
const WeatherStack = createNativeStackNavigator();
const WeatherStackScreen = () => {
  return (
    <WeatherStack.Navigator
      screenOptions={{
        headerShown: false,
      }}
    >
      <WeatherStack.Screen name="HomeScreen" component={HomeScreen} />
      <WeatherStack.Screen name="DetailsScreen" component={DetailsScreen} />
    </WeatherStack.Navigator>
  );
};

const Routes = () => {
  const dispatch = useDispatch();

  useEffect(() => {
    fetchWeather({ dispatch });
  }, []);

  return (
    <NavigationContainer>
      <WeatherStackScreen />
    </NavigationContainer>
  );
};

export default Routes;
