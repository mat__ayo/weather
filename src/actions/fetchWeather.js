import axios from "axios";
import { updateWeatherData, weatherLoading } from "../features/weatherSlice";

// Define a thunk that dispatches those action creators

export const fetchWeather = ({ dispatch }) => {
  const url =
    "https://firebasestorage.googleapis.com/v0/b/navierre-test.appspot.com/o/data.json?alt=media&token=95462ce7-0718-4111-87ca-6c2ae9c78180";

  dispatch(weatherLoading());

  axios
    .get(`${url}`)
    .then((response) => {
      const fetchedWeatherData = response.data;
      dispatch(
        updateWeatherData({
          weatherData: fetchedWeatherData,
        })
      );
    })
    .catch((error) => {
      console.log(`Error: ${error}`);
    });
};
