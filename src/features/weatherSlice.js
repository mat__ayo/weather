import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  weatherLoading: "idle",
  weatherData: {},
};

const weatherSlice = createSlice({
  name: "weather",
  initialState,
  reducers: {
    weatherLoading(state, action) {
      // Use a "state machine" approach for loading state instead of booleans
      if (state.weatherLoading === "idle") {
        state.weatherLoading = "pending";
      }
    },
    updateWeatherData(state, action) {
      if (state.weatherLoading === "pending") {
        state.weatherLoading = "idle";
        state.weatherData = action.payload;
      }
    },
  },
});

export const { weatherLoading, updateWeatherData } = weatherSlice.actions;
export const selectWeatherData = (state) => state.weather.weatherData;

export default weatherSlice.reducer;
