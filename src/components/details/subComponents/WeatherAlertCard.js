import React from "react";
import { View, Text, StyleSheet } from "react-native";

const WeatherAlertCard = ({ alert }) => {
  //-> render alert description
  const renderAlertDescription = () => {
    if (alert) {
      return <Text style={styles.detailAlertText}>{alert.description}</Text>;
    }
  };

  return (
    <View>
      <Text style={styles.detailAlertHeader}>Alerts</Text>
      {renderAlertDescription()}
    </View>
  );
};

const styles = StyleSheet.create({
  detailAlertHeader: {
    fontFamily: "NunitoBold",
    fontSize: 22,
    color: "whitesmoke",
  },
  detailAlertText: {
    marginTop: 10,
    fontFamily: "NunitoRegular",
    fontSize: 16,
    color: "whitesmoke",
    letterSpacing: 1,
  },
});

export default WeatherAlertCard;
