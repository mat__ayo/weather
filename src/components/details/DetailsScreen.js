import React from "react";
import { View, StyleSheet, Text, TouchableOpacity } from "react-native";
import { LinearGradient } from "expo-linear-gradient";

//-> icon imports
import ChevronIcon from "react-native-vector-icons/Feather";

//-> component imports
import WeatherAlertCard from "./subComponents/WeatherAlertCard";
import FloatingDots from "../common/FloatingDots";

const DetailsScreen = ({ route, navigation }) => {
  const { weatherAlerts, otherParams } = route.params;
  console.log("weather alerts", weatherAlerts);
  console.log("other params", otherParams);

  const renderWeatherAlerts = () => {
    return (
      <View style={styles.mechanicContainer}>
        {weatherAlerts &&
          weatherAlerts.map((alert, index) => {
            return <WeatherAlertCard alert={alert} key={index} />;
          })}
      </View>
    );
  };

  const renderMoreParams = () => {
    if (otherParams) {
      const sunriseTime = otherParams.sunrise;
      const sunsetTime = otherParams.sunset;
      const windSpeed = otherParams.windSpeed;

      //-> convert server timestamp to local time
      const convertSunriseTime = () => {
        return new Date(sunriseTime * 1e3).toLocaleTimeString();
      };
      const convertSunsetTime = () => {
        return new Date(sunsetTime * 1e3).toLocaleTimeString();
      };

      return (
        <View>
          <View style={styles.detailTrait}>
            <Text style={styles.detailTraitText}>Sunrise</Text>
            <Text style={styles.detailTraitText}>{convertSunriseTime()}</Text>
          </View>
          <View style={styles.detailTrait}>
            <Text style={styles.detailTraitText}>Sunset</Text>
            <Text style={styles.detailTraitText}>{convertSunsetTime()}</Text>
          </View>
          <View style={styles.detailTrait}>
            <Text style={styles.detailTraitText}>Wind Speed</Text>
            <Text style={styles.detailTraitText}>{windSpeed}km/hr</Text>
          </View>
        </View>
      );
    }
  };

  return (
    <View style={styles.detailsScreenContainer}>
      <LinearGradient
        colors={["#D76192", "#9F91CF", "#9D93D3"]}
        style={styles.GradientScreenContainer}
      >
        {/* Floating Dots */}
        <FloatingDots />

        {/* Custom Back icon button*/}
        <View style={styles.logoContainer}>
          <View style={styles.logoCircleOne}>
            <View style={styles.logoCircleTwo}>
              <View style={styles.logoCircleThree}>
                <TouchableOpacity onPress={() => navigation.goBack()}>
                  <ChevronIcon
                    name="chevron-left"
                    size={40}
                    color="white"
                    style={styles.backIcon}
                  />
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </View>

        <View style={styles.detailsScreenBox}>{renderMoreParams()}</View>
        <View style={styles.detailsAlertBox}>{renderWeatherAlerts()}</View>
      </LinearGradient>
    </View>
  );
};

const styles = StyleSheet.create({
  detailsScreenContainer: {
    flex: 1,
  },
  GradientScreenContainer: {
    flex: 1,
    paddingTop: 40,
    paddingHorizontal: 10,
  },
  logoContainer: {
    flex: 0.5,
  },
  logoCircleOne: {
    borderColor: "white",
    borderWidth: 1,
    width: 60,
    height: 60,
    borderRadius: 60 / 2,
    padding: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  logoCircleTwo: {
    borderColor: "white",
    borderWidth: 1,
    width: 55,
    height: 55,
    borderRadius: 55 / 2,
    padding: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  logoCircleThree: {
    borderColor: "white",
    borderWidth: 1,
    width: 50,
    height: 50,
    borderRadius: 50 / 2,
    padding: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  backIcon: {
    zIndex: 1000,
  },
  detailsScreenBox: {
    flex: 3,
    paddingHorizontal: 30,
    justifyContent: "center",
  },
  detailTrait: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    marginVertical: 10,
  },
  detailTraitText: {
    fontFamily: "NunitoRegular",
    fontSize: 22,
    color: "whitesmoke",
    textAlign: "center",
  },
  detailsAlertBox: {
    flex: 4,
    paddingHorizontal: 30,
  },
});

export default DetailsScreen;
