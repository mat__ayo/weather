import React from "react";
import { View } from "react-native";
import { TouchableOpacity, StyleSheet, Text } from "react-native";

const Button = ({ onPress, text }) => {
  return (
    <TouchableOpacity onPress={onPress} style={styles.buttonContainer}>
      <Text style={styles.buttonText}>{text}</Text>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  buttonContainer: {
    backgroundColor: "#5D4FC1",
    borderRadius: 15,
    padding: 15,
    alignItems: "center",
    justifyContent: "center",
	width: '100%',
  },
  buttonText: {
    fontFamily: "NunitoBold",
    fontSize: 14,
    color: "white",
  },
});

export default Button;
