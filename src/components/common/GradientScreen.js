import React from "react";
import { StyleSheet } from "react-native";
import { LinearGradient } from "expo-linear-gradient";

const GradientScreen = () => {
  return (
    <LinearGradient
      colors={[
        "#D76192",
        "#D36595",
        "#CB6C9F",
        "#C571A5",
        "#BB7AB2",
        "#B57FB8",
        "#AC86C2",
        "#A68BC8",
        "#9F91CF",
        "#9D93D3",
      ]}
      start={{ x: 0, y: 0 }}
      end={{ x: 1, y: 1 }}
      location={[0, 0, 0]}
      style={styles.GradientScreenContainer}
    />
  );
};

const styles = StyleSheet.create({
  GradientScreenContainer: {
    flex: 1,
    paddingTop: 40,
    paddingHorizontal: 10,
  },
});

export default GradientScreen;
