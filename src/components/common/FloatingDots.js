import React from "react";
import { View, StyleSheet } from "react-native";

const FloatingDots = () => {
  return (
    <View style={styles.floatingDotsContainer}>
      <View style={styles.dotOne}></View>
      <View style={styles.dotTwo}></View>
      <View style={styles.dotThree}></View>
      <View style={styles.dotFour}></View>
      <View style={styles.dotFive}></View>
      <View style={styles.dotSix}></View>
      <View style={styles.dotSeven}></View>
    </View>
  );
};

const styles = StyleSheet.create({
  floatingDotsContainer: {
    position: "absolute",
  },
  dotOne: {
    backgroundColor: "white",
    width: 7,
    height: 7,
    borderRadius: 6 / 2,
    marginTop: 20,
    marginLeft: "90%",
    shadowColor: "white",
    shadowOffset: {
      width: 2,
      height: 12,
    },
    shadowOpacity: 0.75,
    shadowRadius: 14.78,
    elevation: 22,
    opacity: 0.7,
  },
  dotTwo: {
    backgroundColor: "white",
    width: 5,
    height: 5,
    borderRadius: 5 / 2,
    marginTop: 20,
    marginLeft: "70%",
    shadowColor: "white",
    shadowOffset: {
      width: 2,
      height: 12,
    },
    shadowOpacity: 0.75,
    shadowRadius: 14.78,
    elevation: 22,
    opacity: 0.7,
  },
  dotThree: {
    backgroundColor: "white",
    width: 8,
    height: 8,
    borderRadius: 8 / 2,
    marginTop: "90%",
    marginLeft: "100.6%",
    shadowColor: "white",
    shadowOffset: {
      width: 2,
      height: 12,
    },
    shadowOpacity: 0.75,
    shadowRadius: 14.78,
    elevation: 22,
    opacity: 0.7,
  },
  dotFour: {
    backgroundColor: "white",
    width: 5,
    height: 5,
    borderRadius: 5 / 2,
    marginTop: "10%",
    marginLeft: "85%",
    shadowColor: "white",
    shadowOffset: {
      width: 2,
      height: 12,
    },
    shadowOpacity: 0.75,
    shadowRadius: 14.78,
    elevation: 22,
    opacity: 0.5,
  },
  dotFive: {
    backgroundColor: "white",
    width: 4,
    height: 4,
    borderRadius: 4 / 2,
    marginTop: "20%",
    marginLeft: "15%",
    shadowColor: "white",
    shadowOffset: {
      width: 2,
      height: 12,
    },
    shadowOpacity: 0.75,
    shadowRadius: 14.78,
    elevation: 22,
    opacity: 0.5,
  },
  dotSix: {
    backgroundColor: "white",
    width: 10,
    height: 10,
    borderRadius: 10 / 2,
    marginTop: "15%",
    marginLeft: "7%",
    shadowColor: "white",
    shadowOffset: {
      width: 2,
      height: 12,
    },
    shadowOpacity: 0.75,
    shadowRadius: 14.78,
    elevation: 22,
    opacity: 0.7,
  },
  dotSeven: {
    backgroundColor: "white",
    width: 6,
    height: 6,
    borderRadius: 6 / 2,
    marginTop: "4%",
    marginLeft: "20%",
    shadowColor: "white",
    shadowOffset: {
      width: 2,
      height: 12,
    },
    shadowOpacity: 0.75,
    shadowRadius: 14.78,
    elevation: 22,
    opacity: 0.5,
  },
});

export default FloatingDots;
