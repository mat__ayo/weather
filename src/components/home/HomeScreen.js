import React from "react";
import { LinearGradient } from "expo-linear-gradient";
import { View, StyleSheet, Text } from "react-native";
import { useSelector } from "react-redux";
import { selectWeatherData } from "../../features/weatherSlice";

//-> component imports
import WeatherCard from "./subComponents/WeatherCard";
import FloatingDots from "../common/FloatingDots";

const HomeScreen = ({ navigation }) => {
  const weatherUpdates = useSelector(selectWeatherData);

  //-> render fetched weather data
  const renderWeatherCard = () => {
    console.log("our weather updates", weatherUpdates);
    const data = weatherUpdates.weatherData;

    if (data) {
      return <WeatherCard weather={data} navigation={navigation} />;
    }
  };

  return (
    <View style={styles.homeScreenContainer}>
      <LinearGradient
        colors={["#D76192", "#9F91CF", "#9D93D3"]}
        style={styles.GradientScreenContainer}
      >
        {/* Floating Dots */}
        <FloatingDots />
        <View style={styles.logoContainer}>
          <View style={styles.logoCircleOne}>
            <View style={styles.logoCircleTwo}>
              <View style={styles.logoCircleThree}>
                <Text style={styles.logoText}>W</Text>
              </View>
            </View>
          </View>
        </View>

        <View style={styles.detailsContainer}>{renderWeatherCard()}</View>
      </LinearGradient>
    </View>
  );
};

const styles = StyleSheet.create({
  homeScreenContainer: {
    flex: 1,
  },
  GradientScreenContainer: {
    flex: 1,
    paddingTop: 40,
    paddingHorizontal: 10,
    position: "relative",
  },
  logoContainer: {
    flex: 2,
  },
  logoCircleOne: {
    borderColor: "white",
    borderWidth: 1,
    width: 60,
    height: 60,
    borderRadius: 60 / 2,
    padding: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  logoCircleTwo: {
    borderColor: "white",
    borderWidth: 1,
    width: 55,
    height: 55,
    borderRadius: 55 / 2,
    padding: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  logoCircleThree: {
    borderColor: "white",
    borderWidth: 1,
    width: 50,
    height: 50,
    borderRadius: 50 / 2,
    padding: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  logoText: {
    fontFamily: "NunitoBold",
    fontSize: 30,
    color: "whitesmoke",
    textAlign: "center",
  },
  detailsContainer: {
    flex: 9,
  },
});

export default HomeScreen;
