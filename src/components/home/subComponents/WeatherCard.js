import React from "react";
import { View, StyleSheet, Text } from "react-native";

//-> icons imports
import WindIcon from "react-native-vector-icons/MaterialCommunityIcons";
import HumidityIcon from "react-native-vector-icons/Ionicons";
import PrecipitationIcon from "react-native-vector-icons/MaterialCommunityIcons";

//-> component imports
import Button from "../../common/Button";

const WeatherCard = ({ weather, navigation }) => {
  //-> get current main weather
  const renderCurrentWeather = () => {
    return (
      <View>
        {weather.current.weather &&
          weather.current.weather.map((today, index) => {
            return (
              <Text style={styles.weatherStatusText} key={index}>
                {today.main}
              </Text>
            );
          })}
      </View>
    );
  };

  //-> get latest time
  const renderTime = () => {
    const today = new Date();
    const time = today.toLocaleDateString("en-us", {
      weekday: "long",
      hour: "numeric",
    });
    return <Text style={styles.timeText}>{time}</Text>;
  };

  //-> render temperature
  const renderTemperature = () => {
    if (weather) {
      const temp = weather.current.temp;
      console.log("temp = ", temp);
      const roundedTemp = Math.round(parseInt(temp));
      if (temp) {
        return <Text style={styles.temperatureText}>{roundedTemp}</Text>;
      }
    }
  };

  return (
    <View style={styles.weatherDetailsCardContainer}>
      <View style={styles.weatherCardContainer}>
        <View style={styles.weatherCardBox}>
          <View>
            {weather.city && weather.state ? (
              <Text style={styles.cityText}>
                {weather.city}, {weather.state}
              </Text>
            ) : null}
          </View>
          <View style={styles.timeTemperatureBox}>
            <View>
              <View style={styles.tempSymbolTextBox}>
                {renderTemperature()}
                <Text style={styles.temperatureSymbol}>c</Text>
              </View>
              {renderTime()}
            </View>
            <View style={styles.weatherStatusBox}>
              {renderCurrentWeather()}
            </View>
          </View>
          <View style={styles.detailsButtonBox}>
            <Button
              text={"Details"}
              onPress={() =>
                navigation.navigate("DetailsScreen", {
                  weatherAlerts: weather.alerts,
                  otherParams: {
                    sunrise: weather.current.sunrise,
                    sunset: weather.current.sunset,
                    windSpeed: weather.current.wind_speed,
                  },
                })
              }
            />
          </View>
        </View>
      </View>

      <View style={styles.weatherTraitsContainer}>
        <View>
          {weather.current.precipitation ? (
            <View style={styles.weatherTraitBox}>
              <PrecipitationIcon name="grain" size={20} color="white" />
              <Text style={styles.weatherTraitText}>Precipitation</Text>
              <Text style={styles.weatherTraitText}>
                {weather.current.precipitation}%
              </Text>
            </View>
          ) : null}

          {weather.current.humidity ? (
            <View style={styles.weatherTraitBox}>
              <HumidityIcon
                name="md-thermometer-outline"
                size={20}
                color="white"
              />
              <Text style={styles.weatherTraitText}>Humidity</Text>
              <Text style={styles.weatherTraitText}>
                {weather.current.humidity}%
              </Text>
            </View>
          ) : null}

          {weather.current.wind_speed ? (
            <View style={styles.weatherTraitBox}>
              <WindIcon name="weather-windy" size={20} color="white" />
              <Text style={styles.weatherTraitText}>Wind</Text>
              <Text style={styles.weatherTraitText}>
                {weather.current.wind_speed}km/h
              </Text>
            </View>
          ) : null}
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  weatherDetailsCardContainer: {
    flex: 1,
  },
  weatherCardContainer: {
    flex: 3.5,
    alignItems: "center",
    justifyContent: "center",
  },
  weatherCardBox: {
    position: "relative",
    backgroundColor: "white",
    borderRadius: 40,
    padding: 10,
    width: "70%",
    height: "95%",
    shadowColor: "#422038",
    shadowOffset: {
      width: 0,
      height: 11,
    },
    shadowOpacity: 0.55,
    shadowRadius: 14.78,
    elevation: 22,
  },
  cityText: {
    fontFamily: "NunitoRegular",
    fontSize: 24,
    color: "#3C4448",
    textAlign: "center",
    paddingVertical: 10,
  },
  timeTemperatureBox: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    paddingTop: 30,
    paddingHorizontal: 10,
  },
  tempSymbolTextBox: {
    flexDirection: "row",
  },
  temperatureText: {
    fontFamily: "NunitoBold",
    fontSize: 40,
    color: "#3C4448",
  },
  temperatureSymbol: {
    fontFamily: "NunitoRegular",
    fontSize: 20,
    color: "#3C4448",
    paddingTop: 5,
    marginLeft: -5,
  },
  timeText: {
    fontFamily: "NunitoRegular",
    fontSize: 16,
    color: "gray",
  },
  weatherStatusBox: {
    backgroundColor: "#D56394",
    borderRadius: 20,
    paddingHorizontal: 20,
    paddingVertical: 2,
  },
  weatherStatusText: {
    fontFamily: "NunitoRegular",
    fontSize: 14,
    color: "white",
  },
  detailsButtonBox: {
    position: "absolute",
    width: "70%",
    bottom: -20,
    alignSelf: "center",
    alignItems: "center",
  },

  weatherTraitsContainer: {
    flex: 3,
    alignItems: "center",
    justifyContent: "center",
  },
  weatherTraitBox: {
    width: "60%",
    flexDirection: "row",
    justifyContent: "space-around",
    alignItems: "center",
    marginVertical: 5,
  },
  weatherTraitText: {
    fontFamily: "NunitoRegular",
    fontSize: 18,
    color: "white",
  },
});

export default WeatherCard;
